import 'package:bakulgincu/view/product/widget/checkout_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'controller/twis_cart_controller.dart';

class TwistCart extends StatefulWidget {
  @override
  _TwistCartState createState() => _TwistCartState();
}

class _TwistCartState extends State<TwistCart> {
  TwistCartController _twistCartController = Get.put(TwistCartController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Get.toNamed('/')),
      ),
      body: (_twistCartController.isLoading.value)
          ? LinearProgressIndicator()
          : ListView.builder(
        itemCount: _twistCartController.carts.length,
        itemBuilder: (context, index) => Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Container(
                    child: Image.network(
                        'https://image.freepik.com/free-photo/top-view-arrangement-with-beauty-bag-copy-space_23-2148301851.jpg'),
                    width: MediaQuery.of(context).size.width * 0.30,
                    height:
                    (_twistCartController.carts[index].title.length >=
                        35)
                        ? MediaQuery.of(context).size.height * 0.25
                        : MediaQuery.of(context).size.height * 0.15,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          bottomLeft: Radius.circular(8)),
                      color: Colors.black12,
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            _twistCartController.carts[index].title,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 16.0),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 8,
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            children: [
                              Center(
                                  child: Text(
                                    NumberFormat.currency(
                                        locale: 'id',
                                        symbol: 'Rp ',
                                        decimalDigits: 0)
                                        .format(_twistCartController
                                        .carts[index].price),
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.0),
                                  )),
                              Center(
                                child: Row(
                                  children: [
                                    IconButton(
                                      icon: Icon(Icons.remove),
                                        onPressed: (){
                                          setState(() {
                                            (_twistCartController
                                                .carts[index].quantity == 1) ? 1 :
                                            _twistCartController
                                                .carts[index].quantity--;
                                          });
                                        }
                                    ),
                                    Text(_twistCartController
                                        .carts[index].quantity
                                        .toString()),
                                    IconButton(
                                      icon: Icon(Icons.add),
                                      onPressed: (){
                                        setState(() {
                                          _twistCartController
                                              .carts[index].quantity++;
                                        });
                                      }
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            children: [
                              Center(
                                  child: Text(
                                    'baru',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.0),
                                  )),
                              Center(
                                  child: Text(
                                    _twistCartController
                                        .carts[index].weight +
                                        ' Kg',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.0),
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ),
                    width: MediaQuery.of(context).size.width * 0.63,
                    height:
                    (_twistCartController.carts[index].title.length >=
                        35)
                        ? MediaQuery.of(context).size.height * 0.25
                        : MediaQuery.of(context).size.height * 0.15,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          bottomRight: Radius.circular(8)),
                      color: Colors.white10,
                    ),
                  ),
                ],
              ),
            )),
      ),
      bottomNavigationBar: _twistCartController.carts.length == 0
          ? Center(
        child: Text(
          'carts kosong, belanja sekarang',
          style: TextStyle(
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.italic),
        ),
      )
          : Container(
        padding: EdgeInsets.all(25),
        decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(13),
                topLeft: Radius.circular(13))),
        child: SafeArea(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "TOTAL",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      NumberFormat.currency(
                          locale: 'id',
                          symbol: 'Rp. ',
                          decimalDigits: 0)
                          .format(_twistCartController.totalPrice),
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: CheckoutButton(
                  _twistCartController.checkOut,
                  "CHECKOUT",
                  Icons.check,
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}

