
import 'package:bakulgincu/view/cart/controller/twis_cart_controller.dart';
import 'package:bakulgincu/view/wishlist/controller/wishlist_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class WishlistPage extends StatelessWidget {
  final TwistWishlistController _twistWishlistController =
      Get.put(TwistWishlistController());
  final TwistCartController _twistCartController =
  Get.put(TwistCartController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: AppBar(
          title: Text('Wishlist'),
        ),
        body: (_twistWishlistController.isLoading.value)
            ? Center(child: CircularProgressIndicator())
            : _twistWishlistController.wishlists.length == 0 ? Center(child: Text('simpan barang incaranmu sekarang', style: TextStyle(fontWeight: FontWeight.w600, fontStyle: FontStyle.italic),)) : StaggeredGridView.countBuilder(
                crossAxisCount: 2,
                itemCount: _twistWishlistController.wishlists.length,
                crossAxisSpacing: 16,
                mainAxisSpacing: 16,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Stack(
                            children: [
                              Container(
                                height: 180,
                                width: double.infinity,
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Image.network(
                                  "https://image.freepik.com/free-photo/top-view-arrangement-with-beauty-bag-copy-space_23-2148301851.jpg",
                                  fit: BoxFit.cover,
                                ),
                              ),
                              GestureDetector(
                                onTap: () async =>
                                    await _twistWishlistController
                                        .removeWishlist(_twistWishlistController
                                            .wishlists
                                            .indexOf(_twistWishlistController
                                                .wishlists[index])),
                                child: SizedBox(
                                  width: 40,
                                  height: 40,
                                  child: FlatButton(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(8),
                                        bottomLeft: Radius.circular(8),
                                      ),
                                    ),
                                    child: Icon(
                                      Icons.delete_forever,
                                      size: 21,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 8),
                          Text(
                            _twistWishlistController.wishlists[index].title,
                            maxLines: 8,
                            style: TextStyle(fontWeight: FontWeight.w800),
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            NumberFormat.currency(
                                    locale: 'id',
                                    symbol: 'Rp ',
                                    decimalDigits: 0)
                                .format(_twistWishlistController
                                    .wishlists[index].price),
                            maxLines: 8,
                            style: TextStyle(fontWeight: FontWeight.w800),
                            overflow: TextOverflow.ellipsis,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                _twistWishlistController
                                    .wishlists[index].locationName,
                                maxLines: 8,
                                style: TextStyle(fontWeight: FontWeight.w800),
                                overflow: TextOverflow.ellipsis,
                              ),
                              GestureDetector(
                                  onTap: () async =>
                                  await _twistCartController.addCart(
                                      _twistWishlistController.wishlists[index]),
                                  child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(5)),
                                        color: Colors.white70,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 3,
                                            blurRadius: 5,
                                            offset: Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      height: 30.0,
                                      width: 30.0,
                                      child: Icon(
                                        Icons.shopping_cart,
                                        color: Colors.pink,
                                      )))
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
                staggeredTileBuilder: (index) => StaggeredTile.fit(1),
              )));
  }
}
