import 'dart:ui';
import 'package:bakulgincu/model/twist/twist_product_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class TwistWishlistController extends GetxController {
  final isWishlist = true.obs;
  var isLoading = true.obs;
  var wishlists = List<TwistProductModel>().obs;

  int get countItem => wishlists.length;
  double get totalPrice => wishlists.fold(0, (sum, item) => sum + item.price);

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchWishlist();
  }

  Future fetchWishlist() async {
    try {
      isLoading(true);
      List<TwistProductModel> wishlistResult = [];
      wishlists.value = wishlistResult;
    } catch (e) {
      print(e);
    } finally {
      isLoading(false);
    }
  }

  addWishlist(TwistProductModel twistProductModel) async {
    try {
      await Future.delayed(Duration(milliseconds: 30))
          .then((value) => wishlists.add(twistProductModel));
    } catch (e) {
      print(e);
    } finally {
    }
  }

  removeWishlist(int twistProductModel) async {
    try {
      await Future.delayed(Duration(milliseconds: 30))
          .then((value) => wishlists.removeAt(twistProductModel));
    } catch (e) {
      print(e);
    } finally {}
  }

  setIsWishlist(bool wishlist){
    isWishlist(wishlist);
  }
}