import 'package:bakulgincu/model/twist/twist_product_model.dart';
import 'package:bakulgincu/view/cart/controller/twis_cart_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailPage extends StatelessWidget {
  final TwistCartController _twistCartController =
      Get.put(TwistCartController());

  final TwistProductModel twistProductModel;

  DetailPage({this.twistProductModel});

  @override
  Widget build(BuildContext context) {
    var item = false;
    return Scaffold(
      appBar: AppBar(title: Text(twistProductModel.title, overflow: TextOverflow.ellipsis,),),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(twistProductModel.title),
            Text(twistProductModel.price.toString()),
            Text(twistProductModel.description),
            Container(),
            RaisedButton(
              child: Text('add to Cart'),
              onPressed: () async =>
                  await _twistCartController.addCart(twistProductModel),
            )
          ],
        ),
      ),
    );
  }
}
