import 'package:bakulgincu/view/cart/controller/twis_cart_controller.dart';
import 'package:bakulgincu/view/login/login_page.dart';
import 'package:bakulgincu/view/product/controller/twist_controller.dart';
import 'package:bakulgincu/view/product/details_page.dart';
import 'package:bakulgincu/view/wishlist/controller/wishlist_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

String finalEmail;

class TwistPage extends StatefulWidget {
  @override
  _TwistPageState createState() => _TwistPageState();
}

class _TwistPageState extends State<TwistPage> {
  final TwistController _twistController = Get.put(TwistController());
  final TwistCartController _twistCartController =
      Get.put(TwistCartController());
  final TwistWishlistController _twistWishlistController =
      Get.put(TwistWishlistController());

  @override
  void initState() {
    // TODO: implement initState
    getValidation().whenComplete(() async => Get.toNamed('/'));
    super.initState();
  }

  Future getValidation() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    var obtainedEmail = sharedPreferences.getString('email');
    setState(() {
      finalEmail = obtainedEmail;
    });
    print(finalEmail);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: AppBar(
          title: Text('Twise Shop'),
          leading: Icon(Icons.menu),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Icon(Icons.shopping_cart),
                    onPressed: () => Get.toNamed('/cartpage'),
                  ),
                  Text(_twistCartController.carts.length.toString()),
                  IconButton(
                    icon: Icon(Icons.thumb_up),
                    onPressed: () => Get.toNamed('/wishlist'),
                  ),
                  Text(_twistWishlistController.wishlists.length.toString()),
                  IconButton(
                    icon: Icon(Icons.person),
                    onPressed: () => Get.toNamed('/login'),
                  ),
                  Text(finalEmail ?? ''),
                  IconButton(
                    icon: Icon(Icons.logout),
                    onPressed: () async {
                      final SharedPreferences sharedpreferences =
                          await SharedPreferences.getInstance();
                      sharedpreferences.remove('email');
                      Get.offNamed('/login');
                    },
                  ),
                ],
              ),
            )
          ],
        ),
        body: (_twistController.isLoading.value)
            ? Center(child: CircularProgressIndicator())
            : Stack(
              children: [
                StaggeredGridView.countBuilder(
                    crossAxisCount: 2,
                    itemCount: _twistController.countItemProduct,
                    crossAxisSpacing: 16,
                    mainAxisSpacing: 16,
                    staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                    itemBuilder: (context, index) {
                      return Card(
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Stack(
                                children: [
                                  Container(
                                    height: 180,
                                    width: double.infinity,
                                    clipBehavior: Clip.antiAlias,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    child: GestureDetector(
                                      onTap: () => Get.to(DetailPage(
                                        twistProductModel:
                                            _twistController.productList[index],
                                      )),
                                      child: Image.network(
                                        "https://image.freepik.com/free-photo/top-view-arrangement-with-beauty-bag-copy-space_23-2148301851.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                      onTap: () async =>
                                          await _twistWishlistController
                                              .addWishlist(_twistController
                                                  .productList[index]),
                                      child: SizedBox(
                                        width: 40,
                                        height: 40,
                                        child: FlatButton(
                                          padding: EdgeInsets.zero,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(8),
                                              bottomLeft: Radius.circular(8),
                                            ),
                                          ),
                                          child: Icon(
                                            Icons.thumb_up,
                                            size: 21,
                                          ),
                                        ),
                                      ))
                                ],
                              ),
                              SizedBox(height: 8),
                              GestureDetector(
                                onTap: () => Get.to(DetailPage(
                                  twistProductModel:
                                      _twistController.productList[index],
                                )),
                                child: Text(
                                  _twistController.productList[index].title,
                                  maxLines: 8,
                                  style: TextStyle(fontWeight: FontWeight.w800),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Text(
                                NumberFormat.currency(
                                        locale: 'id',
                                        symbol: 'Rp ',
                                        decimalDigits: 0)
                                    .format(
                                        _twistController.productList[index].price),
                                maxLines: 8,
                                style: TextStyle(fontWeight: FontWeight.w800),
                                overflow: TextOverflow.ellipsis,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    _twistController
                                        .productList[index].locationName,
                                    maxLines: 8,
                                    style: TextStyle(fontWeight: FontWeight.w800),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  GestureDetector(
                                      onTap: () async =>
                                          await _twistCartController.addCart(
                                              _twistController.productList[index]),
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(5)),
                                            color: Colors.white70,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey.withOpacity(0.5),
                                                spreadRadius: 3,
                                                blurRadius: 5,
                                                offset: Offset(0,
                                                    3), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                          height: 30.0,
                                          width: 30.0,
                                          child: Icon(
                                            Icons.shopping_cart,
                                            color: Colors.pink,
                                          )))
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                Positioned(
                  bottom: 5,
                  right: 10,
                  left: 10,
                  child: Center(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FlatButton(
                              onPressed: () => Get.bottomSheet(Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      topLeft: Radius.circular(8)),
                                  color: Colors.white,
                                ),
                                width: double.infinity,
                                height: 150.0,
                                child: Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceAround,
                                  children: [
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                            onTap: () => _twistController
                                                .productList
                                                .sort((a, b) =>
                                                a.price.compareTo(b.price)),
                                            child: Text('Harga Terendah')),
                                      ),
                                      flex: 1,
                                    ),
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                            onTap: () => _twistController
                                                .productList
                                                .sort((a, b) =>
                                                b.price.compareTo(a.price)),
                                            child: Text('Harga Tertinggi')),
                                      ),
                                      flex: 1,
                                    ),
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                            onTap: () => _twistController
                                                .productList
                                                .sort((a, b) =>
                                                a.title.compareTo(b.title)),
                                            child: Text('Relevan')),
                                      ),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              )),
                              child: Row(
                                children: [
                                  Text('sort'),
                                  Icon(Icons.sort),
                                ],
                              )),
                          SizedBox(
                            child: Container(
                              color: Colors.black,
                              width: 0.5,
                              height: 28,
                            ),
                          ),
                          FlatButton(
                              onPressed: () => Get.bottomSheet(Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      topLeft: Radius.circular(8)),
                                  color: Colors.white,
                                ),
                                width: double.infinity,
                                height: 150.0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('Top Source News'),
                                      ),
                                      flex: 1,
                                    ),
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('Indonesian'),
                                      ),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              )),
                              child: Row(
                                children: [
                                  Icon(Icons.filter_list),
                                  Text('filter'),
                                ],
                              )),
                        ],
                      ),
                      height: 40.0,
                      width: 180.0,
                      decoration: BoxDecoration(
                          color: Colors.white70,
                          borderRadius: BorderRadius.circular(15)),
                    ),
                  ),
                )
              ],
            )));
  }
}
