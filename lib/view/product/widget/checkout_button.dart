import 'package:flutter/material.dart';

class CheckoutButton extends StatelessWidget {

  final Function action;
  final String label;
  final IconData icon;
  CheckoutButton(this.action, this.label, this.icon);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: action,
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100),
      ),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              this.label,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: Colors.lightGreenAccent,
              borderRadius: BorderRadius.circular(40),
            ),
            child: Icon(
              this.icon,
              color: Colors.green,
              size: 16,
            ),
          ),
        ],
      ),
    );
  }
}