import 'package:bakulgincu/model/twist/twist_product_model.dart';
import 'package:bakulgincu/repository/repository.dart';
import 'package:get/get.dart';

class TwistController extends GetxController {
  final RemoteRepository _repository = RemoteRepository();


  final isLoading = true.obs;
  var productList = List<TwistProductModel>().obs;
  int get countItemProduct => productList.length;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchTwistProduct();
  }

  void fetchTwistProduct() async {
    try {
      isLoading(true);
      var products = await _repository.getProductTwist();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}
