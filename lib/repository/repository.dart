
import 'package:bakulgincu/model/twist/twist_product_model.dart';
import 'package:http/http.dart' as http;

class RemoteRepository {
  static var client = http.Client();

  var getTwistProduct =
      'https://ranting.twisdev.com/index.php/rest/items/search/api_key/teampsisthebest/';

  Future<List<TwistProductModel>> getProductTwist() async {
    var response = await client.post(getTwistProduct);
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return twistProductModelFromJson(jsonString);
    } else {
      return null;
    }
  }
}
