import 'package:bakulgincu/view/cart/twist_cart.dart';
import 'package:bakulgincu/view/login/login_page.dart';
import 'package:bakulgincu/view/product/details_page.dart';
import 'package:bakulgincu/view/product/twist_page.dart';
import 'package:bakulgincu/view/sample/test_page.dart';
import 'package:bakulgincu/view/wishlist/wishlist_page.dart';
import 'package:get/get.dart';

routes() => [
  GetPage(name: "/", page: () => TwistPage()),
  GetPage(name: "/cartpage", page: () => TwistCart()),
  GetPage(name: "/detailpage", page: () => DetailPage()),
  GetPage(name: "/wishlist", page: () => WishlistPage()),
  GetPage(name: "/test", page: () => TestPage()),
  GetPage(name: "/login", page: () => LoginPage()),
];