import 'dart:convert';


List<TwistProductModel> twistProductModelFromJson(String str) =>
    List<TwistProductModel>.from(
        json.decode(str).map((x) => TwistProductModel.fromJson(x)));

class TwistProductModel {
  String id;
  String description;
  double price;
  String title;
  String isHalal;
  String weight;
  String locationName;
  Default defaultPhoto;
  ConditionOfItem conditionOfItem;
  int quantity;

  TwistProductModel(
      {this.id,
      this.description,
      this.price,
      this.title,
      this.locationName,
      this.defaultPhoto,
      this.conditionOfItem,
      this.isHalal,
      this.weight,
      this.quantity = 1});

  factory TwistProductModel.fromJson(Map<String, dynamic> json) =>
      TwistProductModel(
        id: json["id"],
        description: json["description"],
        price: double.parse(json["price"].toString()),
        title: json["title"],
        isHalal: json["is_halal"],
        weight: json["weight"],
        locationName: json["location_name"],
        defaultPhoto: Default.fromJson(json["default_photo"]),
        conditionOfItem: ConditionOfItem.fromJson(json["condition_of_item"]),
      );
}

class Default {
  Default({
    this.imgId,
    this.imgParentId,
    this.imgPath,
    this.imgWidth,
    this.imgHeight,
    this.imgDesc,
    this.isEmptyObject,
  });

  String imgId;
  String imgParentId;
  String imgPath;
  String imgWidth;
  String imgHeight;
  String imgDesc;
  String isEmptyObject;

  factory Default.fromJson(Map<String, dynamic> json) => Default(
        imgId: json["img_id"],
        imgParentId: json["img_parent_id"],
        imgPath: json["img_path"],
        imgWidth: json["img_width"],
        imgHeight: json["img_height"],
        imgDesc: json["img_desc"],
        isEmptyObject:
            json["is_empty_object"] == null ? null : json["is_empty_object"],
      );
}

class ConditionOfItem {
  ConditionOfItem({
    this.id,
    this.name,
    this.status,
    this.addedDate,
    this.isEmptyObject,
  });

  String id;
  ConditionOfItemName name;
  String status;
  String addedDate;
  String isEmptyObject;

  factory ConditionOfItem.fromJson(Map<String, dynamic> json) =>
      ConditionOfItem(
        id: json["id"],
        name: conditionOfItemNameValues.map[json["name"]],
        status: json["status"],
        addedDate: json["added_date"],
        isEmptyObject:
            json["is_empty_object"] == null ? null : json["is_empty_object"],
      );
}

enum ConditionOfItemName { BARU, PICKUP_X, EMPTY, TIDAK_BISA_DITAWAR }

final conditionOfItemNameValues = EnumValues({
  "Baru": ConditionOfItemName.BARU,
  "": ConditionOfItemName.EMPTY,
  "Pickup X": ConditionOfItemName.PICKUP_X,
  "Tidak Bisa Ditawar": ConditionOfItemName.TIDAK_BISA_DITAWAR
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
