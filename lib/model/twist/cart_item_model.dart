import 'package:bakulgincu/model/twist/twist_product_model.dart';
import 'package:get/get.dart';

class CartItemModel {
  TwistProductModel product;
  var quantity = 1.obs;

  CartItemModel({this.product, this.quantity});

  incrementQuantity() {
    if (this.quantity >= 10) {
      this.quantity = 10.obs;
    } else {
      this.quantity += 1;
    }
  }

  decrementQuantity() {
    if (this.quantity <= 1) {
      this.quantity = 1.obs;
    } else {
      this.quantity -= 1;
    }
  }
}
